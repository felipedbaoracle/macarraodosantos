FROM openjdk:12
ADD target/docker-spring-boot-felipe_dossantos.jar docker-spring-boot-felipe_dossantos.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-felipe_dossantos.jar"]
